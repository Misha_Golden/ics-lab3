if [ $# -ne 2 ]; then
  echo "Usage: $0 <directory_path> <days>"
  exit 1
fi

dir_path=$1
days=$2

archive_name=$(basename "$dir_path").tar.gz
tar czf "$archive_name" "$dir_path"

echo "Created archive: $archive_name"

find "$dir_path" -type f -mtime +"$days" -exec rm {} \;

deleted_files=$(find "$dir_path" -type f -mtime +"$days" | wc -l)
echo "Deleted $deleted_files files older than $days days"